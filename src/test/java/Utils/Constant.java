package Utils;

import java.util.Random;

import io.restassured.RestAssured;

public class Constant {
	
	public static final String token="eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoic2xpZGluZyIsImV4cCI6MTU3NzI2MDczOSwianRpIjoiZjQzMjk2ZWMyYmZhNDBiMGE2N2EyNDNlMmZkNDM1MjkiLCJyZWZyZXNoX2V4cCI6MTU3NzAwMTUzOSwidXNlcl9pZCI6MTY2fQ.bW_oA1Mo8E6BmKNPNdyi0uUXr4IhbBswgVk37XQ1O6M";
	public static final String baseURI = "https://api-staging.scantist.io/v1";
	public static final String basePath = "/v1/rest-auth/login/";
	public static final String password = "e3bccf84b8b0306366d6922d0dc9a0cbf3b58fd89b2698c509be1f75ea0b54c6";
	public static final String confirmPassword = "e3bccf84b8b0306366d6922d0dc9a0cbf3b58fd89b2698c509be1f75ea0b54c6";
			
	public static String generateRandomUsername() {

		Random randomGenerator = new Random();  
		int randomInt = randomGenerator.nextInt(1000);  
		//System.out.println("scantist"+ randomInt);
		
		return "scantist"+ randomInt;
	}
	
	

}
