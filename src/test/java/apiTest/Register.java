package apiTest;


import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Utils.Constant;
import Utils.RandomEmail;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class Register{

	public static String userName;
	public static Map<String, String> map = new HashMap<String, String>();
	
	@BeforeTest
	public void postdata(){	

		RestAssured.baseURI = Constant.baseURI;
	}
	
	
	//@Test(description="Verify the registartion api with valid request body")
	public void testRegistration(){
		
		RestAssured.basePath = "/rest-auth/registration/";

	userName=Constant.generateRandomUsername();
		
		map.put("username",userName);
        map.put("email",userName+"@gmail.com");
        map.put("password1", Constant.password);
        map.put("password2", Constant.confirmPassword);
		
		Response response = RestAssured.given()
				.contentType("application/json")
				.body(map)
				.when()
				.post()
				.then()
				.statusCode(201)
				.and()
				.extract().response();
		
		System.out.println("/******************************************************************************/");
		System.out.println("Response body ====> " + response.body().asString()); 
		
		JsonPath jsonPathEvaluator = response.jsonPath();
    	String detail = jsonPathEvaluator.get("detail").toString();
    	Assert.assertEquals(detail, "Verification e-mail sent.");
		System.out.println("/******************************************************************************/");

  }
	
}
