package apiTest;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class VerifyJiraConnection {

	@Test(description="Verify the Jira connection api with valid request body")
	public void testJiraConnection(){

		RestAssured.basePath = "/orgs/";
		String orgId="435";

		Response response = RestAssured.given()
				.pathParam("orgId", orgId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{orgId}/jira-connection/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		

		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of Jira connection Api  ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();
		String jiraConnect = jsonPathEvaluator.get("connected").toString();
		String hostName = jsonPathEvaluator.get("host").toString();
	
		if(!jiraConnect.isEmpty() && jiraConnect !=null) {

			System.out.println("jiraConnect is present =====>"+jiraConnect);
			Assert.assertEquals(jiraConnect, "true");
			
				if(!hostName.isEmpty() && hostName !=null) {
					System.out.println("hostName is present =====>"+hostName);
					Assert.assertEquals(hostName, "https://scantist.atlassian.net");

				}
			
		}
	}
	
}
