package apiTest;

import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class AddProjectToGithub {
	
	public static Map<String, String> map = new HashMap<String, String>();


	@Test(description="Verify the add project to github api with valid request body")
	public void testAddProjectToGithub(){
	
        RestAssured.basePath = "/orgs/";
		String orgId= "435";
		
		map.put("activeOrgId", orgId);
		map.put("team", "484");
		map.put("name", "example-python");
		map.put("fullname", "pooja18lh1/example-python");
		map.put("url", "https://github.com/pooja18lh1/example-python.git");
		map.put("description", null);
		map.put("language", null);
		map.put("download_url", "git@github.com:pooja18lh1/example-python.git");
		map.put("private", "false");
		map.put("external_id", "230388046");//https://api.github.com/users/pooja18lh1/repos
		map.put("provider", "github");
		map.put("admin", "true");
		map.put("projectStatus", "notAdded");

	
		Response response = RestAssured.given()
		.pathParam("orgId", orgId)
		.headers("authorization","Token "+Login.token)
		.contentType("application/json")
		.when()
		.post("{orgId}/projects/")
		.then()
		.statusCode(201)
		.and()
		.extract().response();		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of add project to github account ===> " + response.body().asString()); 	
		
		System.out.println("/******************************************************************************/\n");

	}
		
}
