package apiTest;

import static org.testng.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Utils.Constant;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class InviteMemberOrg {

	public static Map<String, String> map = new HashMap<String, String>();
	
	@Test(description="Verify the invite memeber to organization api with valid request body")
	public void testInviteMember(){

		map.put("role","member");
        map.put("email","scantist4@gmail.com");
        
		RestAssured.basePath = "/orgs";
		String orgId="435";
		Response response = RestAssured.given()
		.pathParam("orgId", orgId)
		.headers("authorization","Token "+Login.token)
		.contentType("application/json")
		.body(map)
		.when()
		.post("{orgId}/members/")
		.then()
		.statusCode(201)
		.and()
		.extract().response();	
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body ====> " + response.body().asString());

		System.out.println("/******************************************************************************/\n");

    	JsonPath jsonPathEvaluator = response.jsonPath();
    	String message = jsonPathEvaluator.get("message");
    	Assert.assertEquals(message, "Invite sent", "Correct message received in the Response");

  }
		
}
