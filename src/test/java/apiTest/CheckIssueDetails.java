package apiTest;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class CheckIssueDetails {

	@Test(description="Verify the chaeck issue details api with valid request body")
	public void testGetVulnerabilityResult(){

		RestAssured.basePath = "/issues/";
		String issueId="560047";

		Response response = RestAssured.given()
				.pathParam("issueId", issueId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{issueId}/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		

		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of check the issue details Api  ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();
		String id = jsonPathEvaluator.get("id").toString();
		String module_name = jsonPathEvaluator.get("library_path.module_name").toString();
	
		if(!id.isEmpty() && id !=null) {

			System.out.println("id is present =====>"+id);
			Assert.assertEquals(id, issueId);
			
				if(!module_name.isEmpty() && module_name !=null) {
					System.out.println("module_name is present =====>"+module_name);
				}
			
		}


	}
	
}
