package apiTest;

import org.testng.annotations.Test;

import Utils.Constant;

import org.testng.annotations.BeforeTest;
import static org.hamcrest.Matchers.equalTo;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

import static io.restassured.RestAssured.*;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

public class Login{
	public static Map<String, String> map = new HashMap<String, String>();
	static String token;


	@Test(description="Verify the login api with valid request body")
	public void testLogin(){
		map.put("email","pooja18lh@gmail.com");
        map.put("password","e3bccf84b8b0306366d6922d0dc9a0cbf3b58fd89b2698c509be1f75ea0b54c6");
		
		RestAssured.basePath = "/rest-auth/login/";
		Response response = RestAssured.given()
		.contentType("application/json")
		.body(map)
		.when()
		.post()
		.then()
		.statusCode(200)
		.and()
        .extract().response();
		token = response.getBody().jsonPath().getString("token"); 
		
		System.out.println("/******************************************************************************/\n");
		
		System.out.println("Login Token =====> "+token);	
		
		System.out.println("/******************************************************************************/\n");

   }
	
}
