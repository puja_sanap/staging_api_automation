package apiTest;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class RemoveOrgMember {

	@Test(description="Verify the remove org member api with valid request body")
	public void testRemoveOrgMember(){
	
        RestAssured.basePath = "/orgs/";
		String orgId= "435";
		String memberId= "531";
		Response response = RestAssured.given()
		.pathParam("orgId", orgId)
		.pathParam("memberId", memberId)
		.headers("authorization","Token "+Login.token)
		.contentType("application/json")
		.when()
		.delete("{orgId}/members/{memberId}/")
		.then()
		.statusCode(204)
		.and()
		.extract().response();		
		System.out.println("/******************************************************************************/\n");
		
		System.out.println("Response body of remove Team member ==> " + response.body().asString()); 
		
		System.out.println("/******************************************************************************/\n");

		
	}
	
}
