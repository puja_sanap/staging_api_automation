package apiTest;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class ProjectOverview {
	
	@Test(description="Verify the Project overview api with valid request body")
	public void testgetUsers(){
		
		String orgId = "435";

		RestAssured.basePath = "/orgs/";
		Response response = RestAssured.given()
				.pathParam("orgId", "435")
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{orgId}/projects/overview/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();	
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of Project Overview ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");

		
		JsonPath jsonPathEvaluator = response.jsonPath();
    	String up_to_date = jsonPathEvaluator.get("up-to-date").toString();
    	String outdated = jsonPathEvaluator.get("outdated").toString();
    	String never_scanned = jsonPathEvaluator.get("never-scanned").toString();

    	if(!up_to_date.isEmpty() && up_to_date != null) {
			System.out.println("up_to_date is present =====>"+up_to_date);
		
			if(!outdated.isEmpty() && outdated != null) {
				System.out.println("outdated is present =====>"+outdated);
			
				if(!never_scanned.isEmpty() && never_scanned != null) {
					System.out.println("never_scanned is present =====>"+never_scanned);
				
				}
				
			}
			
		}
	
		
//		String key1="";
//		String key2="";
//		String key3="";
//		
//		if(key1.equalsIgnoreCase("") && key2.equalsIgnoreCase("") && key3.equalsIgnoreCase("")) {
//			
//		}
		
		
		
		
	}
	
}
