package apiTest;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GroupByLicense {
	
	@Test(description="Verify the get Group by license api with valid request body")
	public void testGroupByLicense(){

		RestAssured.basePath = "/orgs/";
		String orgId="435";

		Response response = RestAssured.given()
				.pathParam("orgId", orgId)
				.queryParam("limit",70)
				.queryParam("page",1)
				.queryParam("search","")
				.queryParam("ordering","")
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{orgId}/licenses/group-by-licenses/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		

		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of get Group by license Api  ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();
		String count = jsonPathEvaluator.get("count").toString();
		String license_identifier = jsonPathEvaluator.get("results[0].license_identifier").toString();
		String license_name = jsonPathEvaluator.get("results[0].license_name").toString();


		if(!count.isEmpty() && count !=null) {

			System.out.println("Count is present =====>"+count);

			ArrayList<Object> jsonArray = jsonPathEvaluator.get("results");
			System.out.println("Value of jsonarray size==>" +jsonArray.size());
			if (jsonArray.size()>0) {
				Assert.assertNotNull(jsonArray);

				if(!license_identifier.isEmpty() && license_identifier !=null) {

					System.out.println("license_identifier is present =====>"+license_identifier);
					
					if(!license_name.isEmpty() && license_name !=null) {

						System.out.println("license_name is present =====>"+license_name);
					}
				}
			}
			}
		}
}
