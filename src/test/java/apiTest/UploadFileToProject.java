package apiTest;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class UploadFileToProject {

	public static Map<String, String> map = new HashMap<String, String>();
	public static String projectId;

	@Test(description="Verify the Create project api with valid request body")
	public void testCreateProject(){
	
        RestAssured.basePath = "/orgs/";
		String orgId= "435";
		
		map.put("name","example-javascript-yarn-master");
		map.put("fullname","example-javascript-yarn-master");
		map.put("description","Default team for organization pooja");
		map.put("provider","upload");
		map.put("team","484");
		map.put("external_id","example-javascript-yarn-master5DA05263");
	
		Response response = RestAssured.given()
		.pathParam("orgId", orgId)
		.headers("authorization","Token "+Login.token)
		.contentType("application/json")
		.body(map)
		.when()
		.post("{orgId}/projects/")
		.then()
		.statusCode(200)
		.and()
		.extract().response();		
      
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of Create project ===> " + response.body().asString()); 	
		
		System.out.println("/******************************************************************************/\n");

		
		JsonPath jsonPathEvaluator = response.jsonPath();
    	projectId = jsonPathEvaluator.get("id").toString();
	}
	
	@Test(description="Verify the Upload file to project api with valid request body")
	public void testUploadFile(){
		
        File UploadFile = new File("../staging_api_automation/src/test/resources/UploadFile/Javascript-yarn-master/example-javascript-yarn-master.zip"); //Specify your own location and file
	
        RestAssured.basePath = "/upload/";
		
		Response response = RestAssured.given()
		.multiPart(UploadFile)
		.headers("authorization","Token "+Login.token)
		.contentType("multipart/form-data")
		.when()
		.post()
		.then()
		.statusCode(201)
		.and()
		.extract().response();		
      
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of add project to github account ===> " + response.body().asString()); 	
		
		System.out.println("/******************************************************************************/\n");

				
	}
		

	@Test(description="Verify the Upload file to project api with valid request body")
	public void testUploadFileToProject(){
		String projectId=UploadFileToProject.projectId;
		
		map.put("download_link","example-javascript-yarn-master_QJN15zY.zip");
		map.put("file_size","0.0523454");
		map.put("filename","example-javascript-yarn-master_QJN15zY.zip");
		map.put("file_modified","2019-12-27 15:15:10");
		map.put("version","1.0");
	
        RestAssured.basePath = "/projects/";
		
		Response response = RestAssured.given()
		.pathParams("projectId",projectId)
		.headers("authorization","Token "+Login.token)
		.contentType("application/json")
		.body(map)
		.when()
		.post("{projectId}/uploads/")
		.then()
		.statusCode(201)
		.and()
		.extract().response();		
		
		System.out.println("/******************************************************************************/\n");
      
		System.out.println("Response body of Upload file of project ===> " + response.body().asString()); 	
		
		System.out.println("/******************************************************************************/\n");

		
	}
}
