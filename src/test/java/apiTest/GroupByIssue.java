package apiTest;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GroupByIssue {
	
	@Test(description="Verify the get GroupByIssue api with valid request body")
	public void testGetGroupByIssue(){

		RestAssured.basePath = "/orgs/";
		String orgId="435";

		Response response = RestAssured.given()
				.pathParam("orgId", orgId)
				.queryParam("limit", 70)
				.queryParam("page", 1)
				.queryParam("search", "")
				.queryParam("ordering", "")
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{orgId}/issues/group-by-issues/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of get GroupByIssue Api  ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();
		String count = jsonPathEvaluator.get("count").toString();
		String description = jsonPathEvaluator.get("results.description").toString();
		String project_names = jsonPathEvaluator.get("results.project_names").toString();
		String issue_type = jsonPathEvaluator.get("results.issue_type").toString();


		if(!count.isEmpty() && count !=null) {

			System.out.println("Count is present =====>"+count);

			ArrayList<Object> jsonArray = jsonPathEvaluator.get("results");
			System.out.println("Value of jsonarray size==>" +jsonArray.size());
			if (jsonArray.size()>0) {
				Assert.assertNotNull(jsonArray);

				if(!description.isEmpty() && description !=null) {

					System.out.println("description is present");

					if(!project_names.isEmpty() && project_names !=null) {

						System.out.println("Project Name is present");

						if(!issue_type.isEmpty() && issue_type !=null) {

							System.out.println("Issue Type is present");

						}
					}
				}
			}
		}


	}

}
