package apiTest;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class LicenseOverview {
	
	@Test(description="Verify the License overview api with valid request body")
	public void testgetUsers(){
        String orgId="435";
		
		RestAssured.basePath = "/orgs/";
		Response response = RestAssured.given()
				.pathParam("orgId", orgId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{orgId}/licenses/overview/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of License overview details ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();
		String approve = jsonPathEvaluator.get("approve").toString();
		String deny = jsonPathEvaluator.get("deny").toString();
		String flag = jsonPathEvaluator.get("flag").toString();
	

		if(!approve.isEmpty() && approve !=null) {

			System.out.println("approve is present =====>"+approve);

			if(!deny.isEmpty() && deny !=null) {

				System.out.println("deny is present =====>"+deny);

				if(!flag.isEmpty() && flag !=null) {

					System.out.println("flag is present =====>"+flag);
				}
			}
		}
		
		
	}

}
