package apiTest;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class CreateTeam {

	public static Map<String, String> map = new HashMap<String, String>();
	public static String id;
	
	@Test(description="Verify the create team api with valid request body")
	public void testCreateTeam(){
		
		map.put("name","FrontendTeam");
        map.put("description","FrontendTeam");
        RestAssured.basePath = "/orgs/";
		String orgId="435";
		Response response = RestAssured.given()
		.pathParam("orgId", orgId)
		.headers("authorization","Token "+Login.token)
		.contentType("application/json")
		.body(map)
		.when()
		.post("{orgId}/teams/")
		.then()
		.statusCode(201)
		.and()
		.extract().response();		
      
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body ====> " + response.body().asString()); 
		
		System.out.println("/******************************************************************************/\n");

		JsonPath jsonPathEvaluator = response.jsonPath();
    
		id = jsonPathEvaluator.get("id").toString();
		
    	String name = jsonPathEvaluator.get("name");
    	Assert.assertEquals(name, "FrontendTeam");
    	String organization = jsonPathEvaluator.get("organization");
    	Assert.assertEquals(organization, "435");
    	String description = jsonPathEvaluator.get("description");
    	Assert.assertEquals(description, "FrontendTeam");
  }
}
