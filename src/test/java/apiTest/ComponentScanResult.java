package apiTest;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class ComponentScanResult {

	@Test(description="Verify the component scan result api with valid request body")
	public void testComponentScanResult(){

		RestAssured.basePath = "/scans/";
		String scanId= UploadProjectScan.lastScanid;
		System.out.print("***"+scanId);

		Response response = RestAssured.given()
				.pathParam("scanId", scanId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{scanId}/library-versions/overview/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of component scan result ====> " + response.body().prettyPrint().toString()); 

		System.out.println("/******************************************************************************/\n");

		JsonPath jsonPathEvaluator = response.jsonPath();

		String project_name = jsonPathEvaluator.get("project_name").toString();
		String scan_issues_overview = jsonPathEvaluator.get("scan_issues_overview").toString();
		String license_issues_overview = jsonPathEvaluator.get("license_issues_overview").toString();
		String library_count = jsonPathEvaluator.get("library_count").toString();
		String vulnerable_libraries_count = jsonPathEvaluator.get("vulnerable_libraries_count").toString();
		String library_with_lic_issue = jsonPathEvaluator.get("library_with_lic_issue").toString();


		if(!project_name.isEmpty() && project_name != null) {
			System.out.println("project_name is present =====>"+project_name);

			if(!scan_issues_overview.isEmpty() && scan_issues_overview != null) {
				System.out.println("scan_issues_overview is present =====>"+scan_issues_overview);

				if(!license_issues_overview.isEmpty() && license_issues_overview != null) {
					System.out.println("license_issues_overview is present =====>"+license_issues_overview);

					if(!library_count.isEmpty() && library_count != null) {
						System.out.println("library_count is present =====>"+library_count);

						if(!vulnerable_libraries_count.isEmpty() && vulnerable_libraries_count != null) {
							System.out.println("vulnerable_libraries_count is present =====>"+vulnerable_libraries_count);

							if(!library_with_lic_issue.isEmpty() && library_with_lic_issue != null) {
								System.out.println("library_with_lic_issue is present =====>"+library_with_lic_issue);

							}
						}
					}
				}
			}
		}
	}

}
