package apiTest;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GenerateReport1 {
	
	@Test(description="Verify the generate report1 with valid request body")
	public void testGenerateReport1() throws InterruptedException{


		RestAssured.basePath = "/scans/";
		String scanId= ViewProjectDetails.scanId;


		Response response1 = RestAssured.given()
				.pathParam("scanId", scanId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.post("{scanId}/library-versions/generate/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		
		System.out.println("/******************************************************************************/\n");
		
		System.out.println("Response body of generate report1 ===> " + response1.body().asString()); 
		
		System.out.println("/******************************************************************************/\n");

		JsonPath jsonPathEvaluator = response1.jsonPath();
		
		String detail = jsonPathEvaluator.get("detail").toString();
		Assert.assertEquals(detail, "Started to generate reports.");
		System.out.println("Details of component report==>" +detail);
		}
	}

