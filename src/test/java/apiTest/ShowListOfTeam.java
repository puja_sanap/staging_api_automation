package apiTest;

import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class ShowListOfTeam {
	
	@Test(description="Verify the show list of team api with valid request body")
	public void testShowListOfTeam(){

		RestAssured.basePath = "/orgs/";
		String orgId="435";
		Response response = RestAssured.given()
				.pathParam("orgId", orgId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{orgId}/teams/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		

		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of Show list of Team details ====> " + response.body().prettyPrint().toString()); 

		System.out.println("/******************************************************************************/\n");

		JsonPath jsonPathEvaluator = response.jsonPath();

		ArrayList<Object> jsonArray = jsonPathEvaluator.get("results");
		System.out.println("Value of jsonarray size==>" +jsonArray.size());
		if (jsonArray.size()>=0) {
			Assert.assertNotNull(jsonArray);
			
			int id = jsonPathEvaluator.get("results[0].id");
			String name = jsonPathEvaluator.get("results[0].name");
			String organization = jsonPathEvaluator.get("results[0].organization");
			
			if(id != 0) {
				System.out.println("id is present =====>"+id);
				
				if(!name.isEmpty() && name != null) {
					System.out.println("name is present =====>"+name);
				
					if(!organization.isEmpty() && organization != null) {
						System.out.println("organization is present =====>"+organization);
						
					}
				}
				
			}
		}
		
		
	}
}
