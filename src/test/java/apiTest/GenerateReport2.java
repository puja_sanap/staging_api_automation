package apiTest;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GenerateReport2 {
	
	@Test(description="Verify the generate report2 with valid request body")
	public void testGenerateReport2() throws InterruptedException{


		RestAssured.basePath = "/scans/";
		String scanId= ViewProjectDetails.scanId;
		

		Response response = RestAssured.given()
				.pathParam("scanId", scanId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()				
				.post("{scanId}/issues/generate/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		

		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of generate report2 ===> " + response.body().asString()); 
		
		System.out.println("/******************************************************************************/\n");

		JsonPath jsonPathEvaluator = response.jsonPath();

		
		String detail = jsonPathEvaluator.get("detail").toString();
		Assert.assertEquals(detail, "Started to generate reports.");
		System.out.println("Details of Vulnerability report==>" +detail);
		}
	}

