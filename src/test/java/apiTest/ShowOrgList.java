package apiTest;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class ShowOrgList {

	@Test(description="Verify the show list of team api with valid request body")
	public void testShowOrgList(){

		RestAssured.basePath = "/user/orgs/";
		Response response = RestAssured.given()
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get()
				.then()
				.statusCode(200)
				.and()
				.extract().response();		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of Show list of organization details ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");

		JsonPath jsonPathEvaluator = response.jsonPath();

		ArrayList<Object> jsonArray = jsonPathEvaluator.get("results");
		System.out.println("Value of jsonarray size==>" +jsonArray.size());
		if (jsonArray.size()>0) {
			Assert.assertNotNull(jsonArray);
			
			String ownerName = jsonPathEvaluator.get("results[0].owner_name");
			
			if(!ownerName.isEmpty() && ownerName != null) {
				System.out.println("ownerName is present =====>"+ownerName);

			}
			
		}
	}
	
}
