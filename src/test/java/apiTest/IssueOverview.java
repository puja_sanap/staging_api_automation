package apiTest;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class IssueOverview {

	@Test(description="Verify the get Issue Overview api with valid request body")
	public void testGetIssueOverview(){

		RestAssured.basePath = "/orgs/";
		String orgId="435";

		Response response = RestAssured.given()
				.pathParam("orgId", orgId)
				.queryParam("limit", 70)
				.queryParam("page", 1)
				.queryParam("search", "")
				.queryParam("ordering", "")
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{orgId}/projects/issue-overview")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		

		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of get issue summary Api  ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();
		String count = jsonPathEvaluator.get("count").toString();
		String id = jsonPathEvaluator.get("results.id").toString();
		String name = jsonPathEvaluator.get("results.name").toString();
		String latest_completed_scan_id = jsonPathEvaluator.get("results.latest_completed_scan_id").toString();


		if(!count.isEmpty() && count !=null) {

			System.out.println("Count is present =====>"+count);

			ArrayList<Object> jsonArray = jsonPathEvaluator.get("results");
			System.out.println("Value of jsonarray size==>" +jsonArray.size());
			if (jsonArray.size()>0) {
				Assert.assertNotNull(jsonArray);

				if(!id.isEmpty() && id !=null) {

					System.out.println("Id is present =====>"+id);

					if(!name.isEmpty() && name !=null) {

						System.out.println("Name is present =====>"+name);

						if(!latest_completed_scan_id.isEmpty() && latest_completed_scan_id !=null) {

							System.out.println("Latest completed scan id is present =====>"+latest_completed_scan_id);

						}
					}
				}
			}
		}


	}

}
