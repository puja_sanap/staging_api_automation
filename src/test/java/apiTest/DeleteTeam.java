package apiTest;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class DeleteTeam {
	
	@Test(description="Verify the remove org api with valid request body")
	public void testDeleteTeam(){
		
	
        RestAssured.basePath = "/orgs/";
		String orgId= "435";
		String teamId = CreateTeam.id;

		Response response = RestAssured.given()
		.pathParam("orgId", orgId)
		.pathParam("teamId", teamId)
		.headers("authorization","Token "+Login.token)
		.contentType("application/json")
		.when()
		.delete("{orgId}/teams/{teamId}/")
		.then()
		.statusCode(204)
		.and()
		.extract().response();		
      
		System.out.println("Response body of remove Team  ==> " + response.body().asString()); 
		
	}
	

}
