package apiTest;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class CheckScanStatus {


	@Test(description="Verify the Check scan status with valid request body")
	public void testCheckScanStatus() throws InterruptedException{


		RestAssured.basePath = "/projects/";
		String projectId= "2147";
		String scanId = "11093";
		
		//Thread.sleep(20000);

		Response response = RestAssured.given()
				.pathParam("projectId", projectId)
				.pathParam("scanId", scanId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{projectId}/scans/{scanId}")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		

		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of check scan status ===> " + response.body().asString()); 	
		
		System.out.println("/******************************************************************************/\n");


	

	}
	
}
