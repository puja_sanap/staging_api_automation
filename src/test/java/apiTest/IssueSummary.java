package apiTest;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class IssueSummary {

	@Test(description="Verify the get Issue summary api with valid request body")
	public void testGetIssueSummary(){

		RestAssured.basePath = "/orgs/";
		String orgId="435";

		Response response = RestAssured.given()
				.pathParam("orgId", orgId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{orgId}/issues/issue-summary/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();	
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of get issue summary Api  ====> " + response.body().prettyPrint().toString()); 

		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();
		String cve = jsonPathEvaluator.get("cve").toString();
		String bug = jsonPathEvaluator.get("bug").toString();
		String vcc = jsonPathEvaluator.get("vcc").toString();


		if(!cve.isEmpty() && cve !=null) {

			System.out.println("cve is present =====>"+cve);

			if(!bug.isEmpty() && bug !=null) {

				System.out.println("bug is present =====>"+bug);

				if(!vcc.isEmpty() && vcc !=null) {

					System.out.println("vcc is present =====>"+vcc);
				}
			}
		}
	}


}
