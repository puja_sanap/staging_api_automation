package apiTest;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GroupByLibararyVersion {

	@Test(description="Verify the Group by libarary version api with valid request body")
	public void testGroupByLibararyVersion(){

		RestAssured.basePath = "/orgs/";
		String orgId="435";
	
		Response response = RestAssured.given()
				.pathParam("orgId", orgId)
				.queryParam("limit", 70)
				.queryParam("page", 1)
				.queryParam("search", "")
				.queryParam("ordering", "")

				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{orgId}/library-versions/group-by-library-versions/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		

		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of Group by libarary version  ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();

		String count = jsonPathEvaluator.get("count").toString();
		String versionNumber = jsonPathEvaluator.get("results[0].version_number").toString();
		String library_name = jsonPathEvaluator.get("results[0].library_name").toString();

		
		if(!count.isEmpty() && count != null) {
			System.out.println("Count is present =====>"+count);
			
			ArrayList<Object> jsonArray = jsonPathEvaluator.get("results");
			System.out.println("Value of jsonarray size==>" +jsonArray.size());
			if (jsonArray.size()>0) {
				Assert.assertNotNull(jsonArray);
				
				if(!versionNumber.isEmpty() && versionNumber != null) {
					System.out.println("versionNumber is present =====>"+versionNumber);
					
					if(!library_name.isEmpty() && library_name != null) {
						System.out.println("library_name is present =====>"+library_name);
				
			}
		}
		
		
	}
		}
	}
}
