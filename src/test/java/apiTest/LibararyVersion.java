package apiTest;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class LibararyVersion {

	@Test(description="Verify the get Libarary version api with valid request body")
	public void testLibararyVersionSummary(){

		RestAssured.basePath = "/scans/";
		String scanId=ViewProjectDetails.scanId;
	
		Response response = RestAssured.given()
				.pathParam("scanId", scanId)
				.queryParam("limit", 70)
				.queryParam("page", 1)
				.queryParam("search", "")
				.queryParam("ordering", "")

				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{scanId}/library-versions/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();	
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of Libarary version  ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();

		String count = jsonPathEvaluator.get("count").toString();
		
		if(!count.isEmpty() && count !=null) {
			System.out.println("Count is present =====>"+count);
			
			ArrayList<Object> jsonArray = jsonPathEvaluator.get("results");
			System.out.println("Value of jsonarray size==>" +jsonArray.size());
			if (jsonArray.size()>0) {
				Assert.assertNotNull(jsonArray);
				System.out.println(jsonArray);
				
				String id = jsonPathEvaluator.get("results.id").toString();
				String scan = jsonPathEvaluator.get("results.scan[0]").toString();

				if(!id.isEmpty() && id != null) {
					System.out.println("Id is present =====>"+id);
					
					if(!scan.isEmpty() && scan != null) {
						System.out.println("Scan ID is present =====>"+scan);
						Assert.assertEquals(scan, ViewProjectDetails.scanId);
					}
				}
			}
		}
		
		
	}
	
}
