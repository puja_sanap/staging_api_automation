package apiTest;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class RemoveTeamMember {

	
	@Test(description="Verify the remove team member api with valid request body")
	public void testRemoveTeamMember(){
	
        RestAssured.basePath = "/teams/";
		String teamId= CreateTeam.id;
		String memberId= InviteMemberTeam.memberId;
		
		Response response = RestAssured.given()
		.pathParam("teamId", teamId)
		.pathParam("memberId", memberId)
		.headers("authorization","Token "+Login.token)
		.contentType("application/json")
		.when()
		.delete("{teamId}/members/{memberId}/")
		.then()
		.and()
		.extract().response();	
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of remove Team member ==> " + response.body().asString()); 

		System.out.println("/******************************************************************************/\n");

	}
	
}
