package apiTest;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class ArchivedProject {
	
	public static Map<String, String> map = new HashMap<String, String>();

	@Test(description="Verify the archived project api with valid request body")
	public void testArchivedProject(){
	
        RestAssured.basePath = "/orgs/";
		String orgId= "435";
		String projectId= "2147";
	
		Response response = RestAssured.given()
		.pathParam("orgId", orgId)
		.pathParam("projectId", projectId)
		.headers("authorization","Token "+Login.token)
		.contentType("application/json")
		.when()
		.delete("{orgId}/projects/{projectId}/")
		.then()
		.statusCode(204)
		.and()
		.extract().response();		
		
		System.out.println("/******************************************************************************/\n");
      
		System.out.println("Response body of Archived project===> " + response.body().asString()); 	
		
		System.out.println("/******************************************************************************/\n");

	}
	
	@Test(description="Verify the restore project api with valid request body")
	public void testRestoreProject(){
		
		map.put("archived", "false");
	
        RestAssured.basePath = "/orgs/";
		String orgId= "435";
		String projectId= "2147";
	
		Response response = RestAssured.given()
		.pathParam("orgId", orgId)
		.pathParam("projectId", projectId)
		.headers("authorization","Token "+Login.token)
		.contentType("application/json")
		.body(map)
		.when()
		.patch("{orgId}/projects/{projectId}/")
		.then()
		.statusCode(200)
		.and()
		.extract().response();		
		
		System.out.println("/******************************************************************************/\n");
      
		System.out.println("Response body of restore project===> " + response.body().asString()); 	
		
		System.out.println("/******************************************************************************/\n");

		
		JsonPath jsonPathEvaluator = response.jsonPath();
		//String jsonString = response.asString();
    	String id = jsonPathEvaluator.get("id").toString();
    	Assert.assertEquals(id, "2147");
		System.out.println("Value of Project Id==>" +id); 
		
		String org_id = jsonPathEvaluator.get("org_id").toString();
    	Assert.assertEquals(org_id, "435");
		System.out.println("Value of Org ID==>" +org_id); 
		
		String name = jsonPathEvaluator.get("name").toString();
    	Assert.assertEquals(name, "example-javascript-yarn-master");
		System.out.println("Value of Project name==>" +name); 
		
		String archived = jsonPathEvaluator.get("archived").toString();
    	Assert.assertEquals(archived, "false");
		System.out.println("Value of Archived State==>" +archived); 
	}
}
