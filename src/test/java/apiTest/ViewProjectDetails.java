package apiTest;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class ViewProjectDetails {

	public static String scanId;
	public static String scanStatus;

	@Test(description="Verify the View project details api with valid request body")
	public void testViewProjectDetails(){
		

		RestAssured.basePath = "/projects/";
		String projectId="2147";
		Response response = RestAssured.given()
				.pathParam("projectId", projectId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{projectId}/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of View project details ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();

		scanId = jsonPathEvaluator.get("lastScan.id").toString();
		Assert.assertEquals(scanId, scanId);
		System.out.println("Value of latest Scan id==>" +scanId);

		String id = jsonPathEvaluator.get("id").toString();
		Assert.assertEquals(id, "2147");
		System.out.println("Value of id==>" +id); 

		String name = jsonPathEvaluator.get("name").toString();
		Assert.assertEquals(name, "example-javascript-yarn-master");
		System.out.println("Value of Project name==>" +name); 

		scanStatus = jsonPathEvaluator.get("lastScan.status").toString();
		if(scanStatus.equalsIgnoreCase("queued") || scanStatus.equalsIgnoreCase("running")) {
			Assert.assertEquals(scanStatus, scanStatus);
		}
		System.out.println("Value of Last scan status==>" +scanStatus); 

		String status = jsonPathEvaluator.get("status").toString();
		Assert.assertEquals(status, "up-to-date");
		System.out.println("Value of status==>" +status);

	}

}
