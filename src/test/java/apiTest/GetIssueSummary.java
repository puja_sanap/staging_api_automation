package apiTest;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetIssueSummary {
	
	@Test(description="Verify the get issue summary api with valid request body")
	public void testgetUsers(){
		String orgId = "435";

		RestAssured.basePath = "/orgs/";
		Response response = RestAssured.given()
				.pathParam("orgId", orgId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{orgId}/issues/issue-summary/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of issue summary ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();

		String cve = jsonPathEvaluator.get("cve").toString();
		String bug = jsonPathEvaluator.get("bug").toString();
		String vcc = jsonPathEvaluator.get("vcc").toString();

		if(cve!=null && bug!=null && vcc!=null)
		System.out.println("CVE ==>"+cve);
		System.out.println("BUG ==>"+bug);
		System.out.println("VCC ==>"+vcc);

	}
	

}
