package apiTest;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetKnowledgeGraphData {

	@Test(description="Verify the GetKnowledgeGraphData api with valid request body")
	public void testGetKnowledgeGraphData(){

		RestAssured.basePath = "/orgs/";
		String orgId="435";

		Response response = RestAssured.given()
				.pathParam("orgId", orgId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{orgId}/knowledge-graph/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		

		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of get GetKnowledgeGraphData Api  ====> " + response.body().prettyPrint().toString()); 

		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();
		String id = jsonPathEvaluator.get("nodes[0].id");
		String label = jsonPathEvaluator.get("nodes[0].label");


		ArrayList<Object> jsonArray = jsonPathEvaluator.get("nodes");
		System.out.println("Value of jsonarray size==>" +jsonArray.size());
		if (jsonArray.size()>0) {
			Assert.assertNotNull(jsonArray);

			if(!id.isEmpty() && id != null) {
				System.out.println("id is present =====>"+id);

				if(!label.isEmpty() && label != null) {
					System.out.println("label is present =====>"+label);

				}
			}
		}
	}
}
