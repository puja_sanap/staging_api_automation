package apiTest;

import java.io.FileReader;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class JsonReader{

		    public static void main(String[] args) {
		        JSONParser parser = new JSONParser();
		 
		        try {
		 
		            Object obj = parser.parse(new FileReader("/home/pooja/eclipse-workspace/scantist_API/src/test/resources/TestData/invalidCredentials.json"));
		 
		            JSONObject jsonObject = (JSONObject) obj;
		 
		            String invalidEmail = (String) jsonObject.get("email");
		            String invalidPassword = (String) jsonObject.get("password");
		 
		            System.out.println("email: " + invalidEmail);
		            System.out.println("password: " + invalidPassword);
		           
		          		 
		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		    }
}