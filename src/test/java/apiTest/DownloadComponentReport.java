package apiTest;

import java.util.concurrent.TimeUnit;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class DownloadComponentReport {


	@Test(description="Verify the download component report with valid request body")
	public void testDownloadComponentReportPDF() throws InterruptedException{

		try {
			TimeUnit.MINUTES.sleep(1);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		RestAssured.basePath = "/scans/";
		String scanId= ViewProjectDetails.scanId;
	

		Response response = RestAssured.given()
				.pathParam("scanId", scanId)
				.queryParam("language", "english")
				.queryParam("report_format", "pdf")
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{scanId}/library-versions/export/")				
				.then()
				.statusCode(200)
				.and()
				.extract().response();	
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of Download component report ===> \n/******************************************************/" + response.body().asString()); 
		
		System.out.println("/******************************************************************************/\n");

		
	}
	
	@Test(description="Verify the download component report with valid request body")
	public void testDownloadComponentReportXML() throws InterruptedException{

		try {
			TimeUnit.SECONDS.sleep(10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		RestAssured.basePath = "/scans/";
		String scanId= ViewProjectDetails.scanId;
	

		Response response = RestAssured.given()
				.pathParam("scanId", scanId)
				.queryParam("language", "english")
				.queryParam("report_format", "xml")
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{scanId}/library-versions/export/")				
				.then()
				.statusCode(200)
				.and()
				.extract().response();	
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of Download component report ===> \n/******************************************************/" + response.body().asString()); 
		
		System.out.println("/******************************************************************************/\n");

		
	}
	
	@Test(description="Verify the download component report with valid request body")
	public void testDownloadComponentReportCSV() throws InterruptedException{

		try {
			TimeUnit.SECONDS.sleep(10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		RestAssured.basePath = "/scans/";
		String scanId= ViewProjectDetails.scanId;
	

		Response response = RestAssured.given()
				.pathParam("scanId", scanId)
				.queryParam("language", "english")
				.queryParam("report_format", "csv")
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{scanId}/library-versions/export/")				
				.then()
				.statusCode(200)
				.and()
				.extract().response();	
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of Download component report ===> \n/******************************************************/" + response.body().asString()); 
		
		System.out.println("/******************************************************************************/\n");

		
	}
	
	@Test(description="Verify the download component report with valid request body")
	public void testDownloadComponentReportJSON() throws InterruptedException{

		try {
			TimeUnit.SECONDS.sleep(10);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		RestAssured.basePath = "/scans/";
		String scanId= ViewProjectDetails.scanId;
	

		Response response = RestAssured.given()
				.pathParam("scanId", scanId)
				.queryParam("language", "english")
				.queryParam("report_format", "json")
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{scanId}/library-versions/export/")				
				.then()
				.statusCode(200)
				.and()
				.extract().response();	
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of Download component report ===> \n/******************************************************/" + response.body().asString()); 
		
		System.out.println("/******************************************************************************/\n");

		
	}
	
}
