package apiTest;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class RenameOrganization{

	public static Map<String, String> map = new HashMap<String, String>();
	
	@Test(description="Verify the organization api with valid request body")
	public void testRenameOrganization(){
		String orgId = "435";
		
		map.put("description","updated description");
        map.put("name","pooja");
		RestAssured.basePath = "/orgs/";
		
		Response response = RestAssured.given()
		.pathParam("orgId", orgId)
		.headers("authorization","Token "+Login.token)	
		.contentType("application/json")
		.body(map)
		.when()
		.patch("/{orgId}/")
		.then()
		.statusCode(200)
		.and()
        .extract().response();
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of update organization ====> " + response.body().asString()); 
		
		System.out.println("/******************************************************************************/\n");

        
        JsonPath jsonPathEvaluator = response.jsonPath();
        
    	String id = jsonPathEvaluator.get("id").toString();
    	Assert.assertEquals(id, "435");
    	
    	String name = jsonPathEvaluator.get("name");
    	Assert.assertEquals(name, "pooja");

    	String description = jsonPathEvaluator.get("description");
    	Assert.assertEquals(description, "updated description");
  }
}
