package apiTest;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetOrgLibararySummary {

	
	@Test(description="Verify the get Libarary version summary api with valid request body")
	public void testLibararyVersionSummary(){

		RestAssured.basePath = "/orgs/";
		String orgId="435";
		Response response = RestAssured.given()
				.pathParam("orgId", orgId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{orgId}/library-versions/library-version-summary/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		

		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of Libarary version summary  ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");

		JsonPath jsonPathEvaluator = response.jsonPath();

		String library_versions_total = jsonPathEvaluator.get("library_versions_total").toString();
		
		if(!library_versions_total.isEmpty() && library_versions_total != null) {
			System.out.println("Libarary version total is present =====>"+library_versions_total);
		}
		
		String vulnerable_library_versions = jsonPathEvaluator.get("vulnerable_library_versions").toString();
		
		if(!vulnerable_library_versions.isEmpty() && vulnerable_library_versions != null) {
			System.out.println("Vulnerable library version is present =====>"+vulnerable_library_versions);
			
		}
		
		String library_licenses = jsonPathEvaluator.get("library_licenses").toString();
		if(!library_licenses.isEmpty() && library_licenses != null) {
			System.out.println("library_licenses is present ======>"+library_licenses);
		}
	}
	
}
