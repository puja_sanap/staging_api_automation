package apiTest;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class UploadProjectScan {
	
	public static Map<String, String> map = new HashMap<String, String>();
	public static String lastScanid;

	@Test(description="Verify the Create upload scan with valid request body")
	public void testUploadProjectScan(){

		map.put("scan_type", "source_code");
		map.put("branch", "2196");

		RestAssured.basePath = "/projects/";
		String projectId= "2147";
		

		Response response = RestAssured.given()
				.pathParam("projectId", projectId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.body(map)
				.when()
				.post("{projectId}/scans/")
				.then()
				.statusCode(201)
				.and()
				.extract().response();		

		System.out.println("/******************************************************************************/\n");
		
		System.out.println("Response body of Create upload scan ===> " + response.body().asString()); 	
		
		System.out.println("/******************************************************************************/\n");

		JsonPath jsonPathEvaluator = response.jsonPath();
		  lastScanid = jsonPathEvaluator.get("project.lastScan.id").toString();
		 
			String projectName = jsonPathEvaluator.get("project.name").toString();
			Assert.assertEquals(projectName, "example-javascript-yarn-master");
	}
}
