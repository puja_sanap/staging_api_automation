package apiTest;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetUsers {
	
	@Test(description="Verify the show list of team api with valid request body")
	public void testgetUsers(){

		RestAssured.basePath = "/user/";
		Response response = RestAssured.given()
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get()
				.then()
				.statusCode(200)
				.and()
				.extract().response();		

		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of Show list of users details ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();

		String emailCheck = jsonPathEvaluator.get("email").toString();
		Assert.assertEquals(emailCheck, "pooja18lh@gmail.com");
		
		String subscription = jsonPathEvaluator.get("has_active_subscription").toString();
		Assert.assertEquals(subscription, "true");
	}
	
}
