package apiTest;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import gherkin.deps.com.google.gson.JsonArray;
import groovy.json.JsonException;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GroupScanProject {

	
	@SuppressWarnings("unchecked")
	@Test(description="Verify the add project to github api with valid request body")
	public void testGroupScanProject(){
		
        RestAssured.basePath = "/orgs/";
		String orgId= "435";
		
		//=======
		
		JSONObject childJSON = new JSONObject();
	    childJSON.put("id", 2160);
	    childJSON.put("branch",2220);
	    childJSON.put("scan_type","source_code");
	    
	    JSONObject childJSON1 = new JSONObject();
	    childJSON1.put("id", 2146);
	    childJSON1.put("branch","master");
	    childJSON1.put("scan_type","source_code");

	    JSONArray array = new JSONArray();
	    array.add(childJSON);
	    array.add(childJSON1);
		
	    JSONObject requestParams = new JSONObject();
	    requestParams.put("projects",array);
		
		
		System.out.println("Project array ==>"+requestParams.toJSONString());
	
		Response response = RestAssured.given()
		.pathParam("orgId", orgId)
		.headers("authorization","Token "+Login.token)
		.contentType("application/json")
		.body(requestParams.toJSONString())
		.when()
		.post("{orgId}/projects/group-scan/")
		.then()
		.statusCode(200)
		.and()
		.extract().response();	
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of Group scan Project ===> " + response.body().asString()); 	
		
		System.out.println("/******************************************************************************/\n");

		
		JsonPath jsonPathEvaluator = response.jsonPath();
		
		ArrayList<Object> jsonArray = jsonPathEvaluator.get("projects");
		System.out.println("Value of jsonarray size==>" +jsonArray.size());
		if (jsonArray.size()>0) {
			Assert.assertNotNull(jsonArray);

		String project1 = jsonPathEvaluator.get("projects[0].detail").toString();
		Assert.assertEquals(project1, "Scanned successfully!");
		System.out.println("Value of Project1==>" +project1);
		
		String project2 = jsonPathEvaluator.get("projects[1].detail").toString();
		Assert.assertEquals(project2, "Scanned successfully!");
		System.out.println("Value of Project2==>" +project2);
	}
	}
}
