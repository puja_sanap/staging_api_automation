package apiTest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import Utils.Constant;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class CreateAction {
	
	public static Map<String, String> map = new HashMap<String, String>();


	@Test(description="Verify the CreateAction api with valid request body")
	public void testCreateAction(){

		RestAssured.basePath = "/scans/";
		String scanId= ViewProjectDetails.scanId;
		String issueId = LicenseIssue.issueId;
		
		map.put("scanID", scanId);
		map.put("action_type", "none");
		map.put("priority", "none");
		map.put("assignee", "scantist1");
		map.put("issue_type", "license");
		map.put("issue_id", issueId);

	
		Response response = RestAssured.given()
				.pathParam("scanId", scanId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.body(map)
				.when()
				.post("{scanId}/actions/")
				.then()
				.statusCode(201)
				.and()
				.extract().response();		

		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of CreateAction ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();

		String scan = jsonPathEvaluator.get("scan").toString();
		String reporter = jsonPathEvaluator.get("reporter").toString();
		String action_type = jsonPathEvaluator.get("action_type").toString();
		String priority = jsonPathEvaluator.get("priority").toString();
		String assignee = jsonPathEvaluator.get("assignee").toString();

		

		if(!scan.isEmpty() && scan !=null) {

			System.out.println("scan is present =====>"+scan);
			Assert.assertEquals(scan, scanId);
			if(!reporter.isEmpty() && reporter !=null) {

				System.out.println("reporter is present =====>"+reporter);
				Assert.assertEquals(reporter, "pooja");
				
				if(!action_type.isEmpty() && action_type !=null) {

					System.out.println("action_type is present =====>"+action_type);
						
					if(!priority.isEmpty() && priority !=null) {

						System.out.println("priority is present =====>"+priority);
						
						if(!assignee.isEmpty() && assignee !=null) {

							System.out.println("assignee is present =====>"+assignee);						
					}
					}
				}
			}
		}
	}
	
}
