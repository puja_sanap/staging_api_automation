package apiTest;

import java.util.HashMap;
import java.util.Map;

import org.testng.Assert;
import org.testng.annotations.Test;

import Utils.Constant;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class CreateJiraTicket {
	
	public static Map<String, String> map = new HashMap<String, String>();


	
	@Test(description="Verify the Create JIra tickets with valid request body")
	public void testCreateJiraTickets(){
		
        RestAssured.basePath = "/scans/";
		String scanId= ViewProjectDetails.scanId;
		String issueId= "566704";

		map.put("jira_project_id", "10002");
		map.put("jira_issuetype_name", "Bug");
		map.put("jira_assignee_name", "pooja");
		map.put("jira_description", "[View more on Scantist|https://staging.scantist.io/issue-callback?pid=2147&sid=11323&iid=566704&o=pooja]↵undefined\"");
		map.put("jira_version_ids", "");
		map.put("jira_priority_id", "2");
		map.put("jira_summary", "undefined - [Source:Scantist]");
		
		Response response = RestAssured.given()
		.pathParam("scanId", scanId)
		.pathParam("issueId", issueId)
		.headers("authorization","Token "+Login.token)
		.contentType("application/json")
		.body(map)
		.when()
		.post("{scanId}/issues/{issueId}/jira/")
		.then()
		.statusCode(200)
		.and()
		.extract().response();		
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of Create Jira Ticket ===> " + response.body().asString()); 
		
		System.out.println("/******************************************************************************/\n");

		JsonPath jsonPathEvaluator = response.jsonPath();
	    
		String url = jsonPathEvaluator.get("url").toString();
		if(!url.isEmpty() && url != null) {
			System.out.println("URL is present"+url);
			
		}
		
	}

}
