package apiTest;

import java.util.HashMap;
import java.util.Map;

import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;

import gherkin.deps.com.google.gson.JsonObject;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class InviteMemberTeam {

public static Map<String, String> map = new HashMap<String, String>();

	public static String memberId;
	
	@Test(description="Verify the invite team member api with valid request body")
	public void testinvitememberTeam(){
		map.put("org_member_id","958");
        map.put("role","member");

        RestAssured.basePath = "/teams/";
        
		String teamId=CreateTeam.id;
		
		Response response = RestAssured.given()
		.pathParam("teamId", teamId)
		.headers("authorization","Token "+Login.token)
		.contentType("application/json")
		.body(map)
		.when()
		.post("{teamId}/members/")
		.then()
		.statusCode(201)
		.and()
		.extract().response();		
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body in invite Team member ====> " + response.body().asString()); 
		
		System.out.println("/******************************************************************************/\n");

		
		JsonPath jsonPathEvaluator = response.jsonPath();
		String jsonString = response.asString();
		
		memberId = jsonPathEvaluator.get("id").toString();

		
		String team = jsonPathEvaluator.get("team").toString();
    	Assert.assertEquals(team, teamId);
		System.out.println("Value of Team==>" +team); 
		
		String role = jsonPathEvaluator.get("org_member.role").toString();
    	Assert.assertEquals(role, "member");
		System.out.println("Value of Role==>" +role); 
		
		
    	String state = jsonPathEvaluator.get("org_member.state").toString();
    	Assert.assertEquals(state, "active");
		System.out.println("Value of state==>" +state); 

  }

	
}
