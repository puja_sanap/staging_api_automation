package apiTest;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import org.apache.http.util.TextUtils;
import org.testng.Assert;
import org.testng.annotations.Test;

import Utils.Constant;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetScanResult {

	 
	
	@Test(description="Verify the get scan result api with valid request body")
	public void testGetScanResult(){

		
		RestAssured.basePath = "/scans/";
		String scanId=UploadProjectScan.lastScanid;

		Response response = RestAssured.given()
				.pathParam("scanId", scanId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{scanId}/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();	
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body==== of scan result Api  ====> " + response.body().prettyPrint().toString());
		
		System.out.println("/******************************************************************************/\n");

		JsonPath jsonPathEvaluator = response.jsonPath();
		String project_name = jsonPathEvaluator.get("project_name").toString();
		Assert.assertEquals(project_name, "example-javascript-yarn-master");

		try {
			TimeUnit.MINUTES.sleep(4);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Response response1 = RestAssured.given()
				.pathParam("scanId", scanId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{scanId}/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();	
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body after applying delay==== of scan result Api  ====> " + response1.body().prettyPrint().toString());
		
		System.out.println("/******************************************************************************/\n");

		JsonPath jsonPathEvaluator1 = response1.jsonPath();
		String status1 = jsonPathEvaluator1.get("status").toString();
		Assert.assertEquals(status1, "finished");
		
		String project_name1 = jsonPathEvaluator1.get("project_name").toString();
		Assert.assertEquals(project_name1, "example-javascript-yarn-master");
		
		
		
	}
}



