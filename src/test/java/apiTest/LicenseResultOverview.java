package apiTest;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class LicenseResultOverview {

	@Test(description="Verify the License result overview api with valid request body")
	public void testGetLibraryScanDetails(){

		RestAssured.basePath = "/orgs/";
		String orgId="435";

		Response response = RestAssured.given()
				.pathParam("orgId", orgId)
				.queryParam("limit",70)
				.queryParam("page",1)
				.queryParam("search","")
				.queryParam("ordering","")
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{orgId}/projects/license-result-overview/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();	
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of get License result overview Api  ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();
		int count = jsonPathEvaluator.get("count");
		String result = jsonPathEvaluator.get("results").toString();
		String name = jsonPathEvaluator.get("results[0].name").toString();
		String policy_name = jsonPathEvaluator.get("results[0].policy_name").toString();
		String policy_code = jsonPathEvaluator.get("results[0].policy_code").toString();


		if( count!=0 ) {

			System.out.println("Count is present =====>"+count);

			ArrayList<Object> jsonArray = jsonPathEvaluator.get("results");
			System.out.println("Value of jsonarray size==>" +jsonArray.size());
			if (jsonArray.size()>0) {
				Assert.assertNotNull(jsonArray);
					

				if(!name.isEmpty() && name !=null) {

					System.out.println("name is present =====>"+name);
					

					if(!policy_name.isEmpty() && policy_name !=null) {

						System.out.println("policy_name is present =====>"+policy_name);
						
						if(!policy_code.isEmpty() && policy_code !=null) {

							System.out.println("policy_code is present =====>"+policy_code);
						}
					}
					}
				}
			}
		}
	
}
