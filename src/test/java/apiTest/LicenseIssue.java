package apiTest;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class LicenseIssue {
	
	public static String issueId;
	
	@Test(description="Verify the License issue api with valid request body")
	public void testGetLicenseIssue(){
		

		RestAssured.basePath = "/scans/";
		String scanId=ViewProjectDetails.scanId;

		Response response = RestAssured.given()
				.pathParam("scanId", scanId)
				.queryParam("limit",70)
				.queryParam("page",1)
				.queryParam("search","")
				.queryParam("issue_type","")
				.queryParam("ordering","undefined")
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{scanId}/licenseissues/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		

		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of get License issue  Api  ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();
		String count = jsonPathEvaluator.get("count").toString();
		issueId = jsonPathEvaluator.get("results[0].id").toString();
		String project_name = jsonPathEvaluator.get("results[0].project_name").toString();
		String library_name = jsonPathEvaluator.get("results[0].library_name").toString();
		String library_desc = jsonPathEvaluator.get("results[0].library_desc").toString();


		if(!count.isEmpty() && count !=null) {

			System.out.println("Count is present =====>"+count);

			ArrayList<Object> jsonArray = jsonPathEvaluator.get("results");
			System.out.println("Value of jsonarray size==>" +jsonArray.size());
			if (jsonArray.size()>0) {
				Assert.assertNotNull(jsonArray);
				
				
					

				if(!project_name.isEmpty() && project_name !=null) {

					System.out.println("project_name is present =====>"+project_name);
					

					if(!library_name.isEmpty() && library_name !=null) {

						System.out.println("library_name is present =====>"+library_name);
						
						if(!library_desc.isEmpty() && library_desc !=null) {

							System.out.println("library_desc is present =====>"+library_desc);
						}
					}
					}
				}
			}
		}

}
