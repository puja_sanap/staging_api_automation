package apiTest;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetLibraryScanDetails {

	@Test(description="Verify the get Library scan details api with valid request body")
	public void testGetLibraryScanDetails(){

		RestAssured.basePath = "/library-versions/";
		String issueId="317895";

		Response response = RestAssured.given()
				.pathParam("issueId", issueId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{issueId}/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of get Library scan details Api  ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();
		String id = jsonPathEvaluator.get("id").toString();
		String project_name = jsonPathEvaluator.get("project_name").toString();
		String library_name = jsonPathEvaluator.get("library_version.library.name").toString();


		if(!id.isEmpty() && id !=null) {

			System.out.println("Id is present =====>"+id);
			Assert.assertEquals(id, issueId);

			if(!project_name.isEmpty() && project_name!= null) {
				System.out.println("Project name is present =====>"+project_name);
				Assert.assertEquals(project_name, "example-javascript-yarn-master");

				if(library_name != null && !library_name.isEmpty()) {
					System.out.println("library_name is present =====>"+library_name);
					Assert.assertEquals(library_name, "extend");
				}
			}
		}
	}

}
