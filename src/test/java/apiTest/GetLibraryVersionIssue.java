package apiTest;

import java.util.ArrayList;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class GetLibraryVersionIssue {

	@Test(description="Verify the get Library version issue api with valid request body")
	public void testGetLibraryScanDetails(){

		RestAssured.basePath = "/library-versions/";
		String issueId="317895";

		Response response = RestAssured.given()
				.pathParam("issueId", issueId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.when()
				.get("{issueId}/issues/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of get Library version issue Api  ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();
		String count = jsonPathEvaluator.get("count").toString();
		String library_name1 = jsonPathEvaluator.get("results[0].library_name").toString();
		String library_name2 = jsonPathEvaluator.get("results[1].library_name").toString();


		if(!count.isEmpty() && count !=null) {

			System.out.println("Count is present =====>"+count);

			ArrayList<Object> jsonArray = jsonPathEvaluator.get("results");
			System.out.println("Value of jsonarray size==>" +jsonArray.size());
			if (jsonArray.size()>0) {
				Assert.assertNotNull(jsonArray);

				Assert.assertEquals(library_name1, library_name2);
			}
		}
	}


}
