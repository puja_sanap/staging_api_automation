package apiTest;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class BulkSupress {
	
	@Test(description="Verify the bulk supress api with valid request body")
	public void testBulkSupress(){
		Map<String, int[]> map = new HashMap<String, int[]>();

		RestAssured.basePath = "/scans/";
		String scanId=ViewProjectDetails.scanId;
		int[] val = {558457};

		map.put("scan_issues",val);

		Response response = RestAssured.given()
				.pathParam("scanId", scanId)
				.headers("authorization","Token "+Login.token)
				.contentType("application/json")
				.body(map)
				.when()
				.post("{scanId}/issues/bulk-suppress/")
				.then()
				.statusCode(200)
				.and()
				.extract().response();		
		
		System.out.println("/******************************************************************************/\n");

		System.out.println("Response body of Bulk supress Api  ====> " + response.body().prettyPrint().toString()); 
		
		System.out.println("/******************************************************************************/\n");


		JsonPath jsonPathEvaluator = response.jsonPath();
		String detail = jsonPathEvaluator.get("detail").toString();
		Assert.assertEquals(detail, "Bulk suppression successful");
		
	}
}
